import pyodbc

# Define the connection parameters
server = 'localhost'
database = 'product_inventory'
trusted_connection = 'yes'  # Use Windows Authentication

# Create a connection string
connection_string = f'DRIVER={{ODBC Driver 17 for SQL Server}};SERVER={server};DATABASE={database};Trusted_Connection={trusted_connection};'

try:
    # Connect to the database
    conn = pyodbc.connect(connection_string)
    cursor = conn.cursor()
    
    # Define the SQL command to create the "inventory" table
    create_table_query = """
    CREATE TABLE inventory (
        [product_ID] INT IDENTITY(1, 1) PRIMARY KEY,
        [product_name] NVARCHAR(255) NOT NULL,
        description NVARCHAR(1000),
        category NVARCHAR(255),
        [quantity_in_stock] INT,
        price DECIMAL(10, 2),
        [supplier_name] NVARCHAR(255)
    );
    """

    # Execute the CREATE TABLE query
    cursor.execute(create_table_query)
    
    # Commit the changes
    conn.commit()
    
    print("Table 'inventory' created successfully.")
    
    # Close the connection
    conn.close()
except Exception as e:
    print(f"Table creation error: {str(e)}")
