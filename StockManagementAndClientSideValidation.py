import tkinter as tk
from tkinter import ttk
import pyodbc

# Create a database connection (ensure you have the appropriate connection string)
connection_string = 'Driver={ODBC Driver 17 for SQL Server};Server=localhost;Database=product_inventory;Trusted_Connection=yes;'
conn = pyodbc.connect(connection_string)
cursor = conn.cursor()

def add_record():
    product_name = entry_product_name.get()
    description = entry_description.get()
    category = entry_category.get()
    quantity_in_stock = entry_quantity.get()
    price = entry_price.get()
    supplier_name = entry_supplier_name.get()

    # Validation: Check if required fields are not empty
    if not product_name:
        display_error("Product Name is required.")
        return

    # Check if quantity and price are valid numeric values
    try:
        quantity_in_stock = int(quantity_in_stock)
        price = float(price)
    except ValueError:
        display_error("Quantity and Price must be valid numeric values.")
        return

    # Check if price is greater than zero
    if price <= 0:
        display_error("Price must be greater than zero.")
        return

    # Insert the record into the database
    try:
        insert_query = "INSERT INTO inventory ([product_name], [description], category, [quantity_in_stock], price, [supplier_name]) VALUES (?, ?, ?, ?, ?, ?)"
        cursor.execute(insert_query, (product_name, description, category, quantity_in_stock, price, supplier_name))
        conn.commit()
        clear_fields()
        display_message("Record added successfully.")
    except Exception as e:
        display_error(f"Error adding record: {str(e)}")

def clear_fields():
    entry_product_name.delete(0, 'end')
    entry_description.delete(0, 'end')
    entry_category.delete(0, 'end')
    entry_quantity.delete(0, 'end')
    entry_price.delete(0, 'end')
    entry_supplier_name.delete(0, 'end')

def display_error(message):
    error_label.config(text=message, foreground='red')

def display_message(message):
    error_label.config(text=message, foreground='green')

def on_tooltip(event):
    widget = event.widget
    tooltip = tooltips.get(widget)
    if tooltip:
        display_error(tooltip)

# Create the main window
root = tk.Tk()
root.title("Stock Management Form")

# Create and arrange form elements
frame = ttk.Frame(root)
frame.grid(row=0, column=0, padx=10, pady=10)

label_product_name = ttk.Label(frame, text="product_name:")
label_description = ttk.Label(frame, text="description:")
label_category = ttk.Label(frame, text="category:")
label_quantity_in_stock = ttk.Label(frame, text="quantity_in_stock:")
label_price = ttk.Label(frame, text="price:")
label_supplier_name = ttk.Label(frame, text="supplier_name")

entry_product_name = ttk.Entry(frame)
entry_description = ttk.Entry(frame)
entry_category = ttk.Entry(frame)
entry_quantity = ttk.Entry(frame)
entry_price = ttk.Entry(frame)
entry_supplier_name = ttk.Entry(frame)

button_add = ttk.Button(frame, text="Add Record", command=add_record)

label_product_name.grid(row=0, column=0, sticky='e')
label_description.grid(row=1, column=0, sticky='e')
label_category.grid(row=2, column=0, sticky='e')
label_quantity_in_stock.grid(row=3, column=0, sticky='e')
label_price.grid(row=4, column=0, sticky='e')
label_supplier_name.grid(row=5, column=0, sticky='e')

entry_product_name.grid(row=0, column=1)
entry_description.grid(row=1, column=1)
entry_category.grid(row=2, column=1)
entry_quantity.grid(row=3, column=1)
entry_price.grid(row=4, column=1)
entry_supplier_name.grid(row=5, column=1)

button_add.grid(row=6, column=1)

error_label = ttk.Label(frame, text='', foreground='red')
error_label.grid(row=7, column=0, columnspan=2)

# Create tooltips for entry fields
tooltips = {
    entry_product_name: "Enter the product name.",
    entry_description: "Enter a description.",
    entry_category: "Enter the category.",
    entry_quantity: "Enter the quantity in stock as a positive integer.",
    entry_price: "Enter the price as a positive number greater than zero.",
    entry_supplier_name: "Enter the supplier's name.",
}

for widget, tooltip in tooltips.items():
    widget.bind("<Enter>", on_tooltip)
    widget.bind("<Leave>", lambda event: display_error(""))

# Start the GUI application
root.mainloop()

# Close the database connection when the application is closed
conn.close()
